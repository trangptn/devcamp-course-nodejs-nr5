const mongoose = require('mongoose');
const courseModel = require('../models/courseModel');
// const userModel = require('../models/userModel');

const createCourse = (req, res) => {
    //B1:Thu thap du lieu
    const {
        reqCourseCode,
        reqCourseName,
        reqPrice,
        reqDiscountPrice,
        reqDuration,
        reqLevel,
        reqCoverImage,
        reqTeacherName,
        reqTeacherPhoto,
        isPopular,
        isTrending } = req.body;
    //B2:Validate data
    if (!reqCourseCode) {
        res.status(400).json({
            message: "Course code khong hop le"
        })
        return false;
    }
    if (!reqCourseName) {
        res.status(400).json({
            message: "Course name khong hop le"
        })
        return false;
    }
    if (!reqCoverImage) {
        res.status(400).json({
            message: "Cover image khong hop le"
        })
        return false;
    }
    if (!reqDiscountPrice && Number.isNaN(reqDiscountPrice)) {
        res.status(400).json({
            message: " Discount price khong hop le"
        })
        return false;
    }
    if (!reqDuration) {
        res.status(400).json({
            message: "Duration khong hop le"
        })
        return false;
    }
    if (!reqLevel) {
        res.status(400).json({
            message: "Level khong hop le"
        })
        return false;
    }
    if (!reqPrice && Number.isNaN(reqPrice)) {
        res.status(400).json({
            message: "Price khong hop le"
        })
        return false;
    }
    if (!reqTeacherPhoto) {
        res.status(400).json({
            message: " Teacher photo khong hop le"
        })
        return false;
    }
    if (!reqTeacherName) {
        res.status(400).json({
            message: " Teacher name khong hop le"
        })
        return false;
    }
    if (!isPopular) {
        res.status(400).json({
            message: " popular khong hop le"
        })
        return false;
    }
    if (!isTrending) {
        res.status(400).json({
            message: " Trending khong hop le"
        })
        return false;
    }
   
    //B3:Xu ly va tra ve ket qua
    let newCourse = new courseModel({
        courseCode:reqCourseCode,
        courseName:reqCourseName,
        coverImage:reqCoverImage,
        discountPrice:reqDiscountPrice,
        duration:reqDuration,
        teacherName:reqTeacherName,
        level:reqLevel,
        price:reqPrice,
        teacherPhoto:reqTeacherPhoto,
        isPopular:isPopular,
        isTrending:isTrending
    })

    courseModel.create(newCourse)
            .then((data) => {
                res.status(201).json({
                    message: "Successfully created",
                    result:data
                })
            })
                .catch((err) => {
                    console.log(err);
                    res.status(500).json({
                        message: `Internal sever error: ${err.message}`
                    })
                })
}

const getAllCourse=(req,res) =>{
    //B1: Thu thap du lieu
    //B2:validate data
    //B3: Xu ly va tra ve ket qua
    courseModel.find().exec()
        .then(data=> {
            return res.status(200).json({
                message:"Successfully load all data!",
                course: data
            })
        })
        .catch(error =>{
            return res.status(500).json({
                message:`Internal sever error: ${error.message}`
            })
        })
}
const getCourseById=(req,res)=>{
    //B1: Thu thap du lieu
    var courseid=req.params.courseid;
   //B2:validate data
   if(!mongoose.Types.ObjectId.isValid(courseid))
   {
       return res.status(400).json({
           message:"Id is invalid!"
       })
   }
   //B3: Xu ly va tra ve ket qua
   courseModel.findById(courseid)
       .then(data=>{
           if(!data)
           {
               return res.status(404).json({
                   message:" course not found"
               })
           }
          return res.status(200).json({
           message:"Successfully load data by ID!",
           course: data
          })
       })
       .catch(err=>{
           return res.status(500).json({
               message:`Internal sever error: ${err.message}`
           })
       })
}
const updateCourseById= async (req,res)=>{
    //B1: Thu thap du lieu
    var courseid=req.params.courseid;
    const {
        reqCourseCode,
        reqCourseName,
        reqPrice,
        reqDiscountPrice,
        reqDuration,
        reqLevel,
        reqCoverImage,
        reqTeacherName,
        reqTeacherPhoto,
        isPopular,
        isTrending} = req.body;
   //B2:validate data
   if(!mongoose.Types.ObjectId.isValid(courseid))
   {
       return res.status(400).json({
           message:"Id is invalid!"
       })
   }
   if (reqCourseCode===" ") {
    res.status(400).json({
        message: "Course code khong hop le"
    })
    return false;
}
if (reqCourseName===" ") {
    res.status(400).json({
        message: "Course name khong hop le"
    })
    return false;
}
if (reqCoverImage===" ") {
    res.status(400).json({
        message: "Cover Image khong hop le"
    })
    return false;
}
if (reqDiscountPrice===" ") {
    res.status(400).json({
        message: "Discount price khong hop le"
    })
    return false;
}
if (reqDuration===" ") {
    res.status(400).json({
        message: "Duration khong hop le"
    })
    return false;
}
if (reqLevel===" ") {
    res.status(400).json({
        message: "Level khong hop le"
    })
    return false;
}
if (reqPrice===" ") {
    res.status(400).json({
        message: "Price khong hop le"
    })
    return false;
}
if (reqTeacherName===" ") {
    res.status(400).json({
        message: "Teacher name khong hop le"
    })
    return false;
}
if (reqTeacherPhoto===" ") {
    res.status(400).json({
        message: "Teacher photo khong hop le"
    })
    return false;
}
if (isPopular===" ") {
    res.status(400).json({
        message: "popular khong hop le"
    })
    return false;
}
if (isTrending===" ") {
    res.status(400).json({
        message: "Trending khong hop le"
    })
    return false;
}
   //B3: Xu ly va tra ve ket qua
  try{
    let updateCourse={
        courseCode:reqCourseCode,
        courseName:reqCourseName,
        coverImage:reqCoverImage,
        discountPrice:reqDiscountPrice,
        duration:reqDuration,
        teacherName:reqTeacherName,
        level:reqLevel,
        price:reqPrice,
        teacherPhoto:reqTeacherPhoto,
        isPopular:isPopular,
        isTrending:isTrending
    }
    const updateCo= await courseModel.findByIdAndUpdate(courseid,updateCourse);
    if(updateCo){
        return res.status(200).json({
            message:"Successfully update data by ID!",
            data: updateCo
    })
  }
  else{
        return res.status(404).json({
        message:" Order not found"
        })
  }
}catch(err){
           return res.status(500).json({
               message:`Internal sever error: ${err.message}`
           })
       }
}
const deleteCourseById= async (req,res)=>{
    // B1: Thu thap du lieu
    const courseid = req.params.courseid;
    // B2: Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(courseid)) {
        return res.status(400).json({
            message: "Course ID khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        const deleteCourse= await  courseModel.findByIdAndDelete(courseid)

        if(deleteCourse){
            return res.status(200).json({
                message:"Successfully delete data by ID!",
                data: deleteCourse
               })
        }
        else{
            return res.status(404).json({
                message:" Course not found"
            })
        }
    } catch (error) {
        return res.status(500).json({
        message:`Internal sever error: ${error.message}`
        })
    }
}


module.exports={
    createCourse,
    getAllCourse,
    getCourseById,
    updateCourseById,
    deleteCourseById,
}